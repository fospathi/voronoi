# Module voronoi

Go module voronoi computes [Voronoi tessellations](https://en.wikipedia.org/wiki/Voronoi_diagram).

## Development status

Package voronoi is unstable; expect breaking changes with every commit.

## License

<a>![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/publicdomain.svg){height=75}</a>

SPDX-License-Identifier: CC0-1.0 OR MIT-0

* 
  <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/fospathi/voronoi/">voronoi</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://fospathi.inertialframe.space">Christian Stewart</a> is marked with <a href="http://creativecommons.org/publicdomain/zero/1.0?ref=chooser-v1" target="_blank" rel="license noopener noreferrer">CC0 1.0 Universal</a></p>

* 
    MIT No Attribution

    Copyright 2023 Christian Stewart

    Permission is hereby granted, free of charge, to any person obtaining a copy of this
    software and associated documentation files (the "Software"), to deal in the Software
    without restriction, including without limitation the rights to use, copy, modify,
    merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


