/*
Package boxyvor implements a Voronoi tessellation over a discrete boxy grid.
*/
package boxyvor

import (
	"math"
	"math/rand"
	"sync"
	"time"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/interval"
)

// A Cell encapsulates those grid units closer to its seed than any other seed.
type Cell struct {
	P     maf.Vec2        // Seed position.
	Units dryad.Set[Unit] // Members of the cell. Sometimes cells have no members.
}

// Closest seed to a grid unit.
type Closest struct {
	DSqr float64 // Square of the distance from the center of the grid unit.
	Seed
}

// Geom is the physical and abstract dimensions of a grid.
type Geom struct {
	B      float64 // Box physical width and height.
	GW, GH float64 // Grid physical width and height.
	NX, NY int     // Grid abstract dimensions in terms of numbers of boxes.
}

// Unit containing the given physical grid position.
func (g Geom) Unit(p maf.Vec2) Unit {
	iIn := interval.In{0, float64(g.NX - 1)}
	jIn := interval.In{0, float64(g.NY - 1)}
	return Unit{
		int(iIn.Clamp(float64(g.NX) * p.X / g.GW)),
		int(jIn.Clamp(float64(g.NY) * p.Y / g.GH)),
	}
}

// A Grid of squares over which Voronoi seeds are free to move.
type Grid struct {
	Geom

	Cells map[int]Cell     // Maps seed ID to "boxy but good" Voronoi cells.
	Units map[Unit]Closest // Maps a grid unit to its closest seed.
}

// AnySeed at pseudo-random from the grid's seeds.
func (g Grid) AnySeed() (Seed, bool) {
	for id, c := range g.Cells {
		return Seed{ID: id, P: c.P}, true
	}
	return Seed{}, false
}

// Copy the grid.
func (g Grid) Copy() Grid {
	cells := map[int]Cell{}
	for id, c := range g.Cells {
		c.Units = c.Units.Copy()
		cells[id] = c
	}
	units := map[Unit]Closest{}
	for u, clo := range g.Units {
		units[u] = clo
	}
	return Grid{
		Geom:  g.Geom,
		Cells: cells,
		Units: units,
	}
}

// Equals reports whether the grids are exactly equal in every respect.
func (g Grid) Equals(other Grid) bool {
	if len(g.Cells) != len(other.Cells) || len(g.Units) != len(other.Units) {
		return false
	} else if g.Geom != other.Geom {
		return false
	}

	for id, c := range g.Cells {
		c2, ok := other.Cells[id]
		if !ok {
			return false
		} else if c.P != c2.P {
			return false
		} else if !c.Units.Equals(c2.Units) {
			return false
		}
	}

	for u, clo := range g.Units {
		clo2, ok := other.Units[u]
		if !ok {
			return false
		} else if clo != clo2 {
			return false
		}
	}

	return true
}

// Reposition the seed.
//
// The seed shall already exist on the grid.
//
// To update many seed positions at once it may be faster to use the function
// for repositioning many seeds which makes a new grid from scratch.
func (g Grid) Reposition(id int, p maf.Vec2) {
	c, ok := g.Cells[id]
	if !ok {
		return
	}
	c.P = p
	g.Cells[id] = c
	newSeed := Seed{ID: id, P: p}

	// Update grid unit distances and reallocate grid units lost to other cells.

	invaders := dryad.Set[int]{}
	for u := range c.Units { // Consider neighbour seeds with grid units.
		for nu := range Neighbours(g.Geom, u) {
			invaders.Add(g.Units[nu].ID)
		}
	}
	for id, c := range g.Cells { // Consider any seeds with no grid units.
		if c.Units.IsEmpty() {
			invaders.Add(id)
		}
	}
	invaders.Delete(id)
	for u := range c.Units {
		cen := UnitCentre(g.Geom, u)
		clo := Closest{Seed: newSeed, DSqr: cen.DistToSqd(p)}
		for invID := range invaders {
			invC := g.Cells[invID]
			if dist := cen.DistToSqd(invC.P); dist <= clo.DSqr {
				if dist == clo.DSqr && invID > clo.ID {
					continue
				}
				clo = Closest{Seed: Seed{ID: invID, P: invC.P}, DSqr: dist}
			}
		}
		if clo.ID != id {
			g.Cells[id].Units.Delete(u)
			g.Cells[clo.ID].Units.Add(u)
		}
		g.Units[u] = clo
	}

	// Reallocate grid units gained from other cells starting with the region
	// around the new seed location.

	unvisited := dryad.Set[Unit]{}
	visited := dryad.Set[Unit]{}
	u := g.Unit(p)
	unvisited.Add(u)
	unvisited.AddSet(Neighbours(g.Geom, u))

	for len(unvisited) != 0 {
		for u := range unvisited {
			visited.Add(u)
			unvisited.Delete(u)

			clo := g.Units[u]
			cen := UnitCentre(g.Geom, u)
			dist := cen.DistToSqd(p)
			if dist > clo.DSqr || (dist == clo.DSqr && id > clo.ID) {
				continue
			}
			if clo.ID != id {
				g.Cells[clo.ID].Units.Delete(u)
				g.Cells[id].Units.Add(u)
			}
			clo = Closest{Seed: newSeed, DSqr: dist}
			g.Units[u] = clo

			// It doesn't matter if the map iterates these new keys in this
			// iteration or the next.
			unvisited.AddSet(Neighbours(g.Geom, u).Difference(visited))
		}
	}
}

// A Seed of a Voronoi cell.
//
// A Voronoi cell encapsulates those grid units which are closer to its seed
// than any other seed.
type Seed struct {
	// ID is unique for a seed on a grid.
	ID int
	// P is the physical position of the seed.
	P maf.Vec2
}

// A Unit is a grid unit given by its abstract integral coordinates on the grid.
// Grid unit coords start at 0.
type Unit [2]int

// ClosestCell seed to the given unit.
func ClosestCell(g Geom, cs map[int]Cell, u Unit) Closest {
	var (
		cen = UnitCentre(g, u)
		d   = math.MaxFloat64
		res Seed
	)
	for id, c := range cs {
		if dist := cen.DistToSqd(c.P); dist <= d {
			if dist == d && id > res.ID {
				continue
			}
			d = dist
			res = Seed{ID: id, P: c.P}
		}
	}
	return Closest{d, res}
}

// ClosestSeed to the given unit from the given seeds.
//
// The map of seeds maps seed ID to physical seed position.
func ClosestSeed(g Geom, seeds map[int]maf.Vec2, u Unit) Closest {
	var (
		cen = UnitCentre(g, u)
		d   = math.MaxFloat64
		res Seed
	)
	for id, p := range seeds {
		if dist := cen.DistToSqd(p); dist <= d {
			if dist == d && id > res.ID {
				continue
			}
			d = dist
			res = Seed{ID: id, P: p}
		}
	}
	return Closest{d, res}
}

// Neighbours of the unit including the corners, so up to 8 units.
func Neighbours(g Geom, u Unit) dryad.Set[Unit] {
	x0, y0 := u[0]-1, u[1]-1
	x1, y1 := x0+2, y0+2
	if x0 < 0 {
		x0 = 0
	}
	if y0 < 0 {
		y0 = 0
	}
	if x1 >= g.NX {
		x1 = g.NX - 1
	}
	if y1 >= g.NY {
		y1 = g.NY - 1
	}
	res := dryad.Set[Unit]{}
	for i := x0; i <= x1; i++ {
		for j := y0; j <= y1; j++ {
			res.Add(Unit{i, j})
		}
	}
	res.Delete(u)
	return res
}

// RepositionMany seeds and return a new grid with the changes.
//
// The seeds shall already exist on the grid.
//
// To update just a small number of seed positions it may be faster to use the
// method for moving a single seed.
func RepositionMany(g Grid, seeds map[int]maf.Vec2) Grid {
	s := map[int]maf.Vec2{}
	for id, p := range seeds {
		s[id] = p
	}
	if len(seeds) != len(g.Cells) {
		for id, c := range g.Cells {
			_, ok := s[id]
			if ok {
				continue
			}
			s[id] = c.P
		}
	}
	return NewGrid(GridSpec{
		NX:    g.NX,
		NY:    g.NY,
		Seeds: s,
		W:     g.GW,
	})
}

// UnitCentre of the grid unit with the given coords on the grid with the given
// geometry.
//
// The centre of a grid unit is used when calculating the distance of the grid
// unit from a seed.
func UnitCentre(g Geom, u Unit) maf.Vec2 {
	return maf.Vec2{
		X: float64(u[0])*g.B + g.B/2,
		Y: float64(u[1])*g.B + g.B/2,
	}
}

// GridSpec specifies the properties of a new Grid.
type GridSpec struct {
	// Number of grid units in the X and Y directions.
	NX, NY int
	// Seeds such that keys are seed IDs and values are physical seed positions.
	Seeds map[int]maf.Vec2
	// Physical width of the grid.
	W float64
}

// NewGrid constructs a new grid with the given seed positions.
func NewGrid(spec GridSpec) Grid {
	if spec.NX < 1 {
		spec.NX = 1
	}
	if spec.NY < 1 {
		spec.NY = 1
	}
	if spec.W <= 0 {
		spec.W = 1
	}
	g := Geom{
		B:  spec.W / float64(spec.NX),
		GW: spec.W,
		GH: spec.W * float64(spec.NY) / float64(spec.NX),
		NX: spec.NX,
		NY: spec.NY,
	}

	if len(spec.Seeds) == 0 {
		spec.Seeds = map[int]maf.Vec2{
			0: {X: g.GW / 2, Y: g.GH / 2},
		}
	}

	units := make(map[Unit]Closest, spec.NX*spec.NY)
	cells := make(map[int]Cell, len(spec.Seeds))
	const singleThreadLimit = 25
	if spec.NY < singleThreadLimit {
		for i := 0; i < spec.NX; i++ {
			for j := 0; j < spec.NY; j++ {
				u := Unit{i, j}
				clo := ClosestSeed(g, spec.Seeds, u)
				units[u] = clo
				c, ok := cells[clo.ID]
				if !ok {
					c.P = clo.P
					c.Units = dryad.Set[Unit]{}
				}
				c.Units.Add(u)
				cells[clo.ID] = c
			}
		}
	} else {
		const maxThreads = 8
		threads := maxThreads
		if spec.NY < threads {
			threads = spec.NY
		}
		var h int
		if spec.NY%threads == 0 {
			h = spec.NY / threads
		} else {
			// Make the last thread get a shorter or equal work load.
			h = int(math.Ceil(float64(spec.NY) / float64(threads)))
			threads--
		}
		wg := sync.WaitGroup{}
		wg.Add(threads)

		type gridPortion struct {
			units map[Unit]Closest
			cells map[int]Cell
		}
		portions := make([]gridPortion, threads)
		for n := 0; n < threads; n++ {
			j0, j1 := n*h, n*h+h
			if n == threads-1 {
				j1 = spec.NY
			}
			func(n, j0, j1 int) {
				go func() {
					units := make(map[Unit]Closest, spec.NX*(j1-j0))
					cells := make(map[int]Cell, len(spec.Seeds))
					for i := 0; i < spec.NX; i++ {
						for j := j0; j < j1; j++ {
							u := Unit{i, j}
							clo := ClosestSeed(g, spec.Seeds, u)
							units[u] = clo
							c, ok := cells[clo.ID]
							if !ok {
								c.P = clo.P
								c.Units = dryad.Set[Unit]{}
							}
							c.Units.Add(u)
							cells[clo.ID] = c
						}
					}
					portions[n] = gridPortion{units: units, cells: cells}
					wg.Done()
				}()
			}(n, j0, j1)
		}
		wg.Wait()
		for _, por := range portions {
			for u, clo := range por.units {
				units[u] = clo
			}
			for id, c := range por.cells {
				c2, ok := cells[id]
				if !ok {
					cells[id] = c
				} else {
					c2.Units.AddSet(c.Units)
				}
			}
		}
	}

	if len(cells) != len(spec.Seeds) {
		// Some cell seeds might not have any grid units to which the seed is
		// the closest one.
		for id, p := range spec.Seeds {
			c, ok := cells[id]
			if !ok {
				c.P = p
				c.Units = dryad.Set[Unit]{}
			}
			cells[id] = c
		}
	}

	return Grid{
		Geom:  g,
		Cells: cells,
		Units: units,
	}
}

// RandomGridSpec specifies the properties of a new RandomGrid.
type RandomGridSpec struct {
	// Number of grid units in the X and Y directions.
	NX, NY int
	// Number of seeds.
	Seeds int
	// Physical width of the grid.
	W float64

	// An optional random seed used for generating Voronoi seed positions.
	RandomSeed int64
}

// NewRandomGrid constructs a new Grid with pseudo-randomly positioned seeds.
func NewRandomGrid(spec RandomGridSpec) Grid {
	if spec.NX < 1 {
		spec.NX = 1
	}
	if spec.NY < 1 {
		spec.NY = 1
	}
	if spec.Seeds < 1 {
		spec.Seeds = 1
	}
	if spec.W <= 0 {
		spec.W = 1
	}
	if spec.RandomSeed == 0 {
		spec.RandomSeed = time.Now().UnixNano()
	}
	h := spec.W * float64(spec.NY) / float64(spec.NX)
	g := Geom{
		B:  spec.W / float64(spec.NX),
		GW: spec.W,
		GH: h,
		NX: spec.NX,
		NY: spec.NY,
	}

	rdm := rand.New(rand.NewSource(spec.RandomSeed))
	cells := map[int]Cell{}
	for i := 0; i < spec.Seeds; i++ {
		cells[i] = Cell{
			P: maf.Vec2{
				X: rdm.Float64() * g.GW,
				Y: rdm.Float64() * g.GH,
			},
			Units: dryad.Set[Unit]{},
		}
	}

	units := map[Unit]Closest{}
	for i := 0; i < g.NX; i++ {
		for j := 0; j < g.NY; j++ {
			u := Unit{i, j}
			units[u] = ClosestCell(g, cells, u)
		}
	}

	for u, clo := range units {
		c := cells[clo.ID]
		c.Units.Add(u)
	}

	return Grid{
		Geom:  g,
		Cells: cells,
		Units: units,
	}
}
