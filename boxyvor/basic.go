package boxyvor

import (
	"math"
	"math/rand"
	"sync"
	"time"

	"golang.org/x/exp/slices"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/maf"
)

// A BasicGrid of squares over which Voronoi seeds are free to move.
type BasicGrid struct {
	Geom
	// Seeds of the grid.
	//
	// Slice indices are the seed IDs. Slice values are the physical positions
	// of the seeds.
	Seeds []maf.Vec2
	// Units of the grid.
	//
	// Slice indices can generate the grid unit coordinates. Slice values are
	// the closest seed to the grid unit.
	//
	// Units are in row major order starting with the bottom row of the grid.
	Units []Closest
}

// AnySeed at pseudo-random from the grid's seeds.
func (g *BasicGrid) AnySeed() (Seed, bool) {
	l := len(g.Seeds)
	if l == 0 {
		return Seed{}, false
	}
	id := (<-randomInt) % len(g.Seeds)
	return Seed{ID: id, P: g.Seeds[id]}, true
}

// Cells of the grid's "boxy but good" Voronoi Tessellation such that map keys
// are seed IDs.
//
// Some cells may have no grid units.
func (g *BasicGrid) Cells() map[int]Cell {
	cells := map[int]Cell{}
	for i, l := 0, len(g.Units); i < l; i++ {
		clo := g.Units[i]
		c, ok := cells[clo.ID]
		if !ok {
			c.P = clo.P
			c.Units = dryad.Set[Unit]{}
		}
		c.Units.Add(Unit{i % g.NX, i / g.NX})
		cells[clo.ID] = c
	}
	if len(cells) != len(g.Seeds) { // Empty cells.
		for i, l := 0, len(g.Seeds); i < l; i++ {
			_, ok := cells[i]
			if !ok {
				cells[i] = Cell{
					P:     g.Seeds[i],
					Units: dryad.Set[Unit]{},
				}
			}
		}
	}
	return cells
}

// Copy the grid.
func (g *BasicGrid) Copy() *BasicGrid {
	return &BasicGrid{
		Geom:  g.Geom,
		Seeds: slices.Clone(g.Seeds),
		Units: slices.Clone(g.Units),
	}
}

// Equals reports whether the grids are equal.
func (g *BasicGrid) Equals(other *BasicGrid) bool {
	return g.Geom == other.Geom &&
		slices.Equal(g.Seeds, other.Seeds) &&
		slices.Equal(g.Units, other.Units)
}

// Reposition the seed.
//
// The seed shall already exist on the grid.
//
// To update many seed positions at once it may be faster to use the method
// for repositioning many seeds.
func (g *BasicGrid) Reposition(id int, p maf.Vec2) {

	p0 := g.Seeds[id]
	g.Seeds[id] = p

	// Reallocate grid units lost to other cells.

	unvisited := dryad.Set[Unit]{}
	visited := dryad.Set[Unit]{}
	{
		u := g.Unit(p0)
		unvisited.Add(u)
		unvisited.AddSet(Neighbours(g.Geom, u))
	}

	for len(unvisited) != 0 {
		for u := range unvisited {
			visited.Add(u)
			unvisited.Delete(u)

			ui := u[0] + g.NX*u[1]
			clo := g.Units[ui]
			if clo.ID != id {
				continue
			}
			cen := UnitCentre(g.Geom, u)
			dSqr0 := clo.DSqr
			clo.DSqr = cen.DistToSqd(p)
			clo.P = p

			if clo.DSqr >= dSqr0 {
				for i, l := 0, len(g.Seeds); i < l; i++ {
					pos := g.Seeds[i]
					if dist := cen.DistToSqd(pos); dist > clo.DSqr ||
						(dist == clo.DSqr && i > clo.ID) {
						continue
					} else {
						clo = Closest{DSqr: dist, Seed: Seed{ID: i, P: pos}}
					}
				}
			}

			g.Units[ui] = clo

			// It doesn't matter if the map iterates these new keys in this
			// iteration or the next.
			unvisited.AddSet(Neighbours(g.Geom, u).Difference(visited))
		}
	}

	// Reallocate grid units gained from other cells starting with and spreading
	// out from the region around the new seed location.

	unvisited = dryad.Set[Unit]{}
	visited = dryad.Set[Unit]{}
	{
		u := g.Unit(p)
		unvisited.Add(u)
		unvisited.AddSet(Neighbours(g.Geom, u))
	}

	for len(unvisited) != 0 {
		for u := range unvisited {
			visited.Add(u)
			unvisited.Delete(u)

			ui := u[0] + g.NX*u[1]
			clo := g.Units[ui]
			cen := UnitCentre(g.Geom, u)
			dist := cen.DistToSqd(p)
			if dist > clo.DSqr || (dist == clo.DSqr && id > clo.ID) {
				continue
			}
			clo = Closest{DSqr: dist, Seed: Seed{ID: id, P: p}}
			g.Units[ui] = clo

			// It doesn't matter if the map iterates these new keys in this
			// iteration or the next.
			unvisited.AddSet(Neighbours(g.Geom, u).Difference(visited))
		}
	}
}

// RepositionMany seeds on the grid.
//
// The seeds shall already exist on the grid.
//
// To update just a small number of seed positions it may be faster to use the
// method for repositioning a single seed.
func (g *BasicGrid) RepositionMany(seeds map[int]maf.Vec2) {
	for id, p := range seeds {
		g.Seeds[id] = p
	}

	g2 := NewBasicGrid(BasicGridSpec{
		NX:    g.NX,
		NY:    g.NY,
		Seeds: g.Seeds,
		W:     g.GW,

		units: g.Units,
	})

	g.Units = g2.Units
}

// NearestSeed to the given unit from the given seeds.
func NearestSeed(g Geom, seeds []maf.Vec2, u Unit) Closest {
	var (
		cen = UnitCentre(g, u)
		d   = math.MaxFloat64
		res Seed
	)
	for id, p := range seeds {
		if dist := cen.DistToSqd(p); dist <= d {
			if dist == d && id > res.ID {
				continue
			}
			d = dist
			res = Seed{ID: id, P: p}
		}
	}
	return Closest{d, res}
}

// BasicGridSpec specifies the properties of a new BasicGrid.
type BasicGridSpec struct {
	// Number of grid units in the X and Y directions.
	NX, NY int
	// Seeds such that slice indices are seed IDs and values are physical seed
	// positions.
	Seeds []maf.Vec2
	// Physical width of the grid.
	W float64

	units []Closest
}

// NewBasicGrid constructs a new basic grid with the given seed positions.
func NewBasicGrid(spec BasicGridSpec) *BasicGrid {
	if spec.NX < 1 {
		spec.NX = 1
	}
	if spec.NY < 1 {
		spec.NY = 1
	}
	if spec.W <= 0 {
		spec.W = 1
	}
	g := Geom{
		B:  spec.W / float64(spec.NX),
		GW: spec.W,
		GH: spec.W * float64(spec.NY) / float64(spec.NX),
		NX: spec.NX,
		NY: spec.NY,
	}

	if len(spec.Seeds) == 0 {
		spec.Seeds = []maf.Vec2{{X: g.GW / 2, Y: g.GH / 2}}
	}

	var units []Closest
	if len(spec.units) != 0 {
		units = spec.units
	} else {
		units = make([]Closest, spec.NX*spec.NY)
	}
	const singleThreadLimit = 25
	if spec.NY < singleThreadLimit {
		for i := 0; i < spec.NX*spec.NY; i++ {
			units[i] = NearestSeed(g, spec.Seeds, Unit{i % g.NX, i / g.NX})
		}
	} else {
		const maxThreads = 8
		threads := maxThreads
		if spec.NY < threads {
			threads = spec.NY
		}
		h := spec.NY / threads
		if spec.NY%threads != 0 {
			// Make the last thread get a shorter or equal work load.
			h = int(math.Ceil(float64(spec.NY) / float64(threads)))
			threads--
		}
		wg := sync.WaitGroup{}
		wg.Add(threads)

		for n := 0; n < threads; n++ {
			i0, i1 := spec.NX*h*n, spec.NX*(h*n+h)
			if n == threads-1 {
				i1 = spec.NX * spec.NY
			}
			func(n, i0, i1 int) {
				go func() {
					for i := i0; i < i1; i++ {
						units[i] = NearestSeed(
							g, spec.Seeds, Unit{i % g.NX, i / g.NX},
						)
					}
					wg.Done()
				}()
			}(n, i0, i1)
		}
		wg.Wait()
	}

	return &BasicGrid{
		Geom:  g,
		Seeds: spec.Seeds,
		Units: units,
	}
}

// NewRandomBasicGrid constructs a new BasicGrid with pseudo-randomly positioned
// seeds.
func NewRandomBasicGrid(spec RandomGridSpec) BasicGrid {
	if spec.NX < 1 {
		spec.NX = 1
	}
	if spec.NY < 1 {
		spec.NY = 1
	}
	if spec.Seeds < 1 {
		spec.Seeds = 1
	}
	if spec.W <= 0 {
		spec.W = 1
	}
	if spec.RandomSeed == 0 {
		spec.RandomSeed = time.Now().UnixNano()
	}
	h := spec.W * float64(spec.NY) / float64(spec.NX)
	g := Geom{
		B:  spec.W / float64(spec.NX),
		GW: spec.W,
		GH: h,
		NX: spec.NX,
		NY: spec.NY,
	}

	rdm := rand.New(rand.NewSource(spec.RandomSeed))
	seeds := make([]maf.Vec2, spec.Seeds)
	for i := 0; i < spec.Seeds; i++ {
		seeds[i] = maf.Vec2{
			X: rdm.Float64() * g.GW,
			Y: rdm.Float64() * g.GH,
		}
	}

	units := make([]Closest, spec.NX*spec.NY)
	for i := 0; i < spec.NX*spec.NY; i++ {
		u := Unit{i % spec.NX, i / spec.NX}
		cen := UnitCentre(g, u)
		clo := Closest{DSqr: math.MaxFloat64, Seed: Seed{ID: math.MaxInt}}
		for j := 0; j < spec.Seeds; j++ {
			p := seeds[j]
			dist := cen.DistToSqd(p)
			if dist > clo.DSqr || (dist == clo.DSqr && j > clo.ID) {
				continue
			}
			clo = Closest{DSqr: dist, Seed: Seed{ID: j, P: p}}
		}
		units[i] = clo
	}

	return BasicGrid{
		Geom:  g,
		Seeds: seeds,
		Units: units,
	}
}

var randomInt = func() <-chan int {
	ch := make(chan int, 100)
	go func() {
		rdm := rand.New(rand.NewSource(time.Now().UnixNano()))
		for {
			ch <- rdm.Int()
		}
	}()
	return ch
}()
