package boxyvor_test

import (
	"fmt"
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/voronoi/boxyvor"
)

func BenchmarkBasicGrid_Reposition(b *testing.B) {
	g := boxyvor.NewRandomGrid(boxyvor.RandomGridSpec{
		NX:    40,
		NY:    40,
		W:     1,
		Seeds: 40,
	})
	for n := 0; n < b.N; n++ {
		s1, _ := g.AnySeed()
		s2, _ := g.AnySeed()
		g.Reposition(s1.ID, s1.P.MidTo(s2.P))
	}
}

func BenchmarkBasicGrid_RepositionMany(b *testing.B) {
	b.Run("large", func(b *testing.B) {
		g := boxyvor.NewRandomBasicGrid(boxyvor.RandomGridSpec{
			NX:    1000,
			NY:    1000,
			W:     1,
			Seeds: 200,
		})
		for n := 0; n < b.N; n++ {
			s1, _ := g.AnySeed()
			s2, _ := g.AnySeed()
			g.RepositionMany(map[int]maf.Vec2{
				s1.ID: s1.P.MidTo(s2.P),
			})
		}
	})

	b.Run("half & double", func(b *testing.B) {
		g := boxyvor.NewRandomBasicGrid(boxyvor.RandomGridSpec{
			NX:    500,
			NY:    500,
			W:     1,
			Seeds: 400,
		})
		for n := 0; n < b.N; n++ {
			s1, _ := g.AnySeed()
			s2, _ := g.AnySeed()
			g.RepositionMany(map[int]maf.Vec2{
				s1.ID: s1.P.MidTo(s2.P),
			})
		}
	})

	b.Run("small", func(b *testing.B) {
		g := boxyvor.NewRandomBasicGrid(boxyvor.RandomGridSpec{
			NX:    160,
			NY:    80,
			W:     1,
			Seeds: 400,
		})
		for n := 0; n < b.N; n++ {
			s1, _ := g.AnySeed()
			s2, _ := g.AnySeed()
			g.RepositionMany(map[int]maf.Vec2{
				s1.ID: s1.P.MidTo(s2.P),
			})
		}
	})
}

func TestBasicGrid_Reposition(t *testing.T) {
	//  == Physical = | = Centre = | = Abstract =
	//  2.00 - 2.25   | 2.125      | 8
	//  1.75 - 2.0    | 1.875      | 7
	//  1.50 - 1.75   | 1.625      | 6
	//	1.25 - 1.50   | 1.375      | 5
	//  1.00 - 1.25   | 1.125      | 4
	//  0.75 - 1.00   | 0.875      | 3
	//  0.50 - 0.75   | 0.625      | 2
	//  0.25 - 0.50   | 0.375      | 1
	//  0.00 - 0.25   | 0.125      | 0

	const nx, ny = 4, 9
	g := boxyvor.Geom{B: 0.25, GW: 1, GH: 2.25, NX: nx, NY: ny}
	s1 := boxyvor.Seed{ID: 0, P: maf.Vec2{X: 0.5, Y: 1.875}}
	s2 := boxyvor.Seed{ID: 1, P: maf.Vec2{X: 0.5, Y: 0.375}}
	units := make([]boxyvor.Closest, nx*ny)

	for i := 0; i < nx*ny; i++ {
		u := boxyvor.Unit{i % nx, i / nx}
		if i < 4*nx {
			units[i] = boxyvor.Closest{
				DSqr: boxyvor.UnitCentre(g, u).DistToSqd(s2.P),
				Seed: s2,
			}
		} else {
			units[i] = boxyvor.Closest{
				DSqr: boxyvor.UnitCentre(g, u).DistToSqd(s1.P),
				Seed: s1,
			}
		}
	}

	gr := boxyvor.BasicGrid{
		Geom:  g,
		Seeds: []maf.Vec2{s1.P, s2.P},
		Units: units,
	}
	cells := gr.Cells()

	want, got := true, len(cells[s1.ID].Units) == nx*5
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(cells[s2.ID].Units) == nx*4
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test moving the upper seed higher. The upper cell loses a row of grid
	// units to the lower cell.

	gr.Reposition(s1.ID, maf.Vec2{X: 0.5, Y: 1.9})
	cells = gr.Cells()

	want, got = true, len(cells[s1.ID].Units) == nx*4
	fmt.Printf("len1: %v\n", len(cells[s1.ID].Units))
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(cells[s2.ID].Units) == nx*5
	fmt.Printf("len2: %v\n", len(cells[s2.ID].Units))
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test moving the upper seed lower.

	gr.Reposition(s1.ID, maf.Vec2{X: 0.5, Y: 0.4})
	cells = gr.Cells()

	want, got = true, len(cells[s1.ID].Units) == nx*7
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(cells[s2.ID].Units) == nx*2
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test moving the upper seed higher again.

	gr.Reposition(s1.ID, maf.Vec2{X: 0.5, Y: 1.9})
	cells = gr.Cells()

	want, got = true, len(cells[s1.ID].Units) == nx*4
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(cells[s2.ID].Units) == nx*5
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test seeds at the grid corners.

	gr.Reposition(s1.ID, maf.Vec2{X: 1, Y: 2.25})
	gr.Reposition(s2.ID, maf.Vec2{X: 0, Y: 0})
	cells = gr.Cells()

	want, got = true, len(cells[s1.ID].Units) == (nx*ny)/2 &&
		cells[s1.ID].Units.Contains(boxyvor.Unit{3, 8})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(cells[s2.ID].Units) == (nx*ny)/2 &&
		cells[s2.ID].Units.Contains(boxyvor.Unit{0, 0})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test two coinciding seeds.

	gr.Reposition(s1.ID, maf.Vec2{X: 0.5, Y: 0.5})
	gr.Reposition(s2.ID, maf.Vec2{X: 0.5, Y: 0.5})
	cells = gr.Cells()

	want, got = true, len(cells[s1.ID].Units) == nx*ny
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(cells[s2.ID].Units) == 0
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestBasicGrid_RepositionMany(t *testing.T) {
	g1 := boxyvor.NewRandomBasicGrid(boxyvor.RandomGridSpec{
		NX:    20,
		NY:    20,
		Seeds: 10,
		W:     1,
	})
	midpoint := maf.Vec2{X: 0.5, Y: 0.5}

	s, _ := g1.AnySeed()

	g2 := g1.Copy()
	g2.RepositionMany(map[int]maf.Vec2{
		s.ID: midpoint,
	})

	want, got := true, !g1.Equals(g2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	g1.Reposition(s.ID, midpoint)

	want, got = true, g1.Equals(g2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	s1, _ := g1.AnySeed()
	s2, _ := g1.AnySeed()

	g2.RepositionMany(map[int]maf.Vec2{
		s1.ID: {X: 0, Y: 0},
		s2.ID: {X: 1, Y: 1},
	})

	want, got = true, !g1.Equals(g2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	g1.Reposition(s1.ID, maf.Vec2{X: 0, Y: 0})
	g1.Reposition(s2.ID, maf.Vec2{X: 1, Y: 1})

	want, got = true, g1.Equals(g2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewBasicGrid(t *testing.T) {
	g := boxyvor.NewBasicGrid(boxyvor.BasicGridSpec{
		NX: 2,
		NY: 2,
		Seeds: []maf.Vec2{
			{X: 1.5, Y: 0.5},
			{X: 1.5, Y: 1.5},
			{X: 1.7, Y: 0.5}, // No grid units.
			{X: 1.7, Y: 0.5}, // No grid units.
		},
		W: 2,
	})

	// Check the new basic grid maker accounts for seeds with no grid units.

	cells := g.Cells()

	want, got := true, len(cells) == 4 &&
		len(cells[2].Units) == 0 && cells[2].Units != nil &&
		len(cells[3].Units) == 0 && cells[3].Units != nil
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test multithreaded grid creation.

	g = boxyvor.NewBasicGrid(boxyvor.BasicGridSpec{
		NX: 100,
		NY: 100,
		Seeds: []maf.Vec2{
			{X: 1.99, Y: 0.5},
			{X: 1.99, Y: 1.5},
			{X: 1.995, Y: 0.5}, // No grid units.
			{X: 1.995, Y: 0.5}, // No grid units.
		},
		W: 2,
	})

	cells = g.Cells()

	want, got = true, len(cells) == 4 &&
		len(g.Units) == 100*100 &&
		len(cells[0].Units) == (100*100/2) &&
		len(cells[1].Units) == (100*100/2) &&
		len(cells[2].Units) == 0 && cells[2].Units != nil &&
		len(cells[3].Units) == 0 && cells[3].Units != nil
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewRandomBasicGrid(t *testing.T) {
	const (
		xUnits = 3
		yUnits = 6
		seeds  = 5
		physW  = 10
	)
	g := boxyvor.NewRandomBasicGrid(boxyvor.RandomGridSpec{
		NX:    xUnits,
		NY:    yUnits,
		Seeds: seeds,
		W:     physW,
	})

	// Test the number of grid units is the product of the width and height in
	// grid units.

	want, got := xUnits*yUnits, len(g.Units)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test a grid has a number of cells equal to the number of seeds asked for.

	cells := g.Cells()

	want, got = seeds, len(cells)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test the seed positions are constrained to be within the physical grid
	// dimensions.

	wIn := interval.In{0, physW}
	hIn := interval.In{0, physW * float64(yUnits) / float64(xUnits)}
	for _, c := range cells {
		p := c.P
		want, got := true,
			wIn.AsClosedContains(p.X) && hIn.AsClosedContains(p.Y)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test all grid units are accounted for in the Voronoi tessellation.

	{
		var n int
		for _, c := range cells {
			vu := c.Units
			n += len(vu)
		}
		want, got := xUnits*yUnits, n
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
