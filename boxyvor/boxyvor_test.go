package boxyvor_test

import (
	"testing"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/voronoi/boxyvor"
)

func BenchmarkGrid_Reposition(b *testing.B) {
	g := boxyvor.NewRandomGrid(boxyvor.RandomGridSpec{
		NX:    40,
		NY:    40,
		W:     1,
		Seeds: 40,
	})
	for n := 0; n < b.N; n++ {
		s1, _ := g.AnySeed()
		s2, _ := g.AnySeed()
		g.Reposition(s1.ID, s1.P.MidTo(s2.P))
	}
}

func BenchmarkRepositionMany(b *testing.B) {
	b.Run("large", func(b *testing.B) {
		g := boxyvor.NewRandomGrid(boxyvor.RandomGridSpec{
			NX:    1000,
			NY:    1000,
			W:     1,
			Seeds: 200,
		})
		for n := 0; n < b.N; n++ {
			s1, _ := g.AnySeed()
			s2, _ := g.AnySeed()
			boxyvor.RepositionMany(g, map[int]maf.Vec2{
				s1.ID: s1.P.MidTo(s2.P),
			})
		}
	})

	b.Run("half & double", func(b *testing.B) {
		g := boxyvor.NewRandomGrid(boxyvor.RandomGridSpec{
			NX:    500,
			NY:    500,
			W:     1,
			Seeds: 400,
		})
		for n := 0; n < b.N; n++ {
			s1, _ := g.AnySeed()
			s2, _ := g.AnySeed()
			boxyvor.RepositionMany(g, map[int]maf.Vec2{
				s1.ID: s1.P.MidTo(s2.P),
			})
		}
	})

	b.Run("small", func(b *testing.B) {
		g := boxyvor.NewRandomGrid(boxyvor.RandomGridSpec{
			NX:    160,
			NY:    80,
			W:     1,
			Seeds: 400,
		})
		for n := 0; n < b.N; n++ {
			s1, _ := g.AnySeed()
			s2, _ := g.AnySeed()
			boxyvor.RepositionMany(g, map[int]maf.Vec2{
				s1.ID: s1.P.MidTo(s2.P),
			})
		}
	})
}

func TestClosestCell(t *testing.T) {
	//  == Physical = | = Centre = | = Abstract =
	//  2.00 - 2.25   | 2.125      | 8
	//  1.75 - 2.0    | 1.875      | 7
	//  1.50 - 1.75   | 1.625      | 6
	//	1.25 - 1.50   | 1.375      | 5
	//  1.00 - 1.25   | 1.125      | 4
	//  0.75 - 1.00   | 0.875      | 3
	//  0.50 - 0.75   | 0.625      | 2
	//  0.25 - 0.50   | 0.375      | 1
	//  0.00 - 0.25   | 0.125      | 0
	g := boxyvor.Geom{B: 0.25, GW: 1, GH: 2.25, NX: 4, NY: 9}
	s1 := boxyvor.Seed{ID: 0, P: maf.Vec2{X: 0.5, Y: 1.875}}
	s2 := boxyvor.Seed{ID: 1, P: maf.Vec2{X: 0.5, Y: 0.375}}

	// Test the bulk of the units are closest to the top seed with the lower ID.
	// Equal distance ties are resolved by IDs.

	ids := dryad.Set[int]{}
	for i := 0; i < 4; i++ {
		for j := 4; j < 9; j++ {
			id := boxyvor.ClosestCell(g, map[int]boxyvor.Cell{
				s1.ID: {P: s1.P},
				s2.ID: {P: s2.P},
			}, boxyvor.Unit{i, j}).ID
			ids.Add(id)
		}
	}

	want, got := true, len(ids) == 1 && ids.Contains(s1.ID)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test the bottom one quarter of the units are closest to the bottom seed.

	ids = dryad.Set[int]{}
	for i := 0; i < 4; i++ {
		for j := 0; j < 4; j++ {
			id := boxyvor.ClosestCell(g, map[int]boxyvor.Cell{
				s1.ID: {P: s1.P},
				s2.ID: {P: s2.P},
			}, boxyvor.Unit{i, j}).ID
			ids.Add(id)
		}
	}

	want, got = true, len(ids) == 1 && ids.Contains(s2.ID)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestGrid_Reposition(t *testing.T) {
	//  == Physical = | = Centre = | = Abstract =
	//  2.00 - 2.25   | 2.125      | 8
	//  1.75 - 2.0    | 1.875      | 7
	//  1.50 - 1.75   | 1.625      | 6
	//	1.25 - 1.50   | 1.375      | 5
	//  1.00 - 1.25   | 1.125      | 4
	//  0.75 - 1.00   | 0.875      | 3
	//  0.50 - 0.75   | 0.625      | 2
	//  0.25 - 0.50   | 0.375      | 1
	//  0.00 - 0.25   | 0.125      | 0

	const nx, ny = 4, 9
	g := boxyvor.Geom{B: 0.25, GW: 1, GH: 2.25, NX: nx, NY: ny}
	s1 := boxyvor.Seed{ID: 0, P: maf.Vec2{X: 0.5, Y: 1.875}}
	s2 := boxyvor.Seed{ID: 1, P: maf.Vec2{X: 0.5, Y: 0.375}}

	upper := dryad.Set[boxyvor.Unit]{}
	lower := dryad.Set[boxyvor.Unit]{}
	units := map[boxyvor.Unit]boxyvor.Closest{}

	for i := 0; i < nx; i++ {
		for j := 4; j < ny; j++ {
			u := boxyvor.Unit{i, j}
			units[u] = boxyvor.Closest{
				DSqr: boxyvor.UnitCentre(g, u).DistToSqd(s1.P),
				Seed: s1,
			}
			upper.Add(u)
		}
	}

	for i := 0; i < nx; i++ {
		for j := 0; j < 4; j++ {
			u := boxyvor.Unit{i, j}
			units[u] = boxyvor.Closest{
				DSqr: boxyvor.UnitCentre(g, u).DistToSqd(s2.P),
				Seed: s2,
			}
			lower.Add(u)
		}
	}

	gr := boxyvor.Grid{
		Geom: g,
		Cells: map[int]boxyvor.Cell{
			s1.ID: {P: s1.P, Units: upper},
			s2.ID: {P: s2.P, Units: lower},
		},
		Units: units,
	}

	want, got := true, len(gr.Cells[s1.ID].Units) == nx*5
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(gr.Cells[s2.ID].Units) == nx*4
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test moving the upper seed higher. The upper cell loses a row of grid
	// units to the lower cell.

	gr.Reposition(s1.ID, maf.Vec2{X: 0.5, Y: 1.9})

	want, got = true, len(gr.Cells[s1.ID].Units) == nx*4
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(gr.Cells[s2.ID].Units) == nx*5
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test moving the upper seed lower.

	gr.Reposition(s1.ID, maf.Vec2{X: 0.5, Y: 0.4})

	want, got = true, len(gr.Cells[s1.ID].Units) == nx*7
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(gr.Cells[s2.ID].Units) == nx*2
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test moving the upper seed higher again.

	gr.Reposition(s1.ID, maf.Vec2{X: 0.5, Y: 1.9})

	want, got = true, len(gr.Cells[s1.ID].Units) == nx*4
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(gr.Cells[s2.ID].Units) == nx*5
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test seeds at the grid corners.

	gr.Reposition(s1.ID, maf.Vec2{X: 1, Y: 2.25})
	gr.Reposition(s2.ID, maf.Vec2{X: 0, Y: 0})

	want, got = true, len(gr.Cells[s1.ID].Units) == (nx*ny)/2 &&
		gr.Cells[s1.ID].Units.Contains(boxyvor.Unit{3, 8})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(gr.Cells[s2.ID].Units) == (nx*ny)/2 &&
		gr.Cells[s2.ID].Units.Contains(boxyvor.Unit{0, 0})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test two coinciding seeds.

	gr.Reposition(s1.ID, maf.Vec2{X: 0.5, Y: 0.5})
	gr.Reposition(s2.ID, maf.Vec2{X: 0.5, Y: 0.5})

	want, got = true, len(gr.Cells[s1.ID].Units) == nx*ny
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = true, len(gr.Cells[s2.ID].Units) == 0
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNeighbours(t *testing.T) {
	g := boxyvor.Geom{
		B:  1,
		GW: 5, GH: 5,
		NX: 5, NY: 5,
	}

	// Test the corner positions.

	neigh := boxyvor.Neighbours(g, boxyvor.Unit{0, 0})

	want, got := true, len(neigh) == 3 && neigh.ContainsAll([]boxyvor.Unit{
		{0, 1}, {1, 1}, {1, 0},
	}...)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	neigh = boxyvor.Neighbours(g, boxyvor.Unit{0, 4})

	want, got = true, len(neigh) == 3 && neigh.ContainsAll([]boxyvor.Unit{
		{0, 3}, {1, 3}, {1, 4},
	}...)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	neigh = boxyvor.Neighbours(g, boxyvor.Unit{4, 4})

	want, got = true, len(neigh) == 3 && neigh.ContainsAll([]boxyvor.Unit{
		{3, 4}, {3, 3}, {4, 3},
	}...)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	neigh = boxyvor.Neighbours(g, boxyvor.Unit{4, 0})

	want, got = true, len(neigh) == 3 && neigh.ContainsAll([]boxyvor.Unit{
		{3, 0}, {3, 1}, {4, 1},
	}...)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test a central position.

	neigh = boxyvor.Neighbours(g, boxyvor.Unit{2, 2})

	want, got = true, len(neigh) == 8 && neigh.ContainsAll([]boxyvor.Unit{
		{1, 3}, {2, 3}, {3, 3},
		{1, 2}, {3, 2},
		{1, 1}, {2, 1}, {3, 1},
	}...)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test a single row grid.

	g = boxyvor.Geom{
		B:  1,
		GW: 5, GH: 1,
		NX: 5, NY: 1,
	}

	neigh = boxyvor.Neighbours(g, boxyvor.Unit{2, 0})

	want, got = true, len(neigh) == 2 && neigh.ContainsAll([]boxyvor.Unit{
		{1, 0}, {3, 0},
	}...)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test a single entry grid.

	gr := boxyvor.NewRandomGrid(boxyvor.RandomGridSpec{})
	neigh = boxyvor.Neighbours(gr.Geom, boxyvor.Unit{0, 0})

	want, got = true, len(neigh) == 0
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewGrid(t *testing.T) {
	g := boxyvor.NewGrid(boxyvor.GridSpec{
		NX: 2,
		NY: 2,
		Seeds: map[int]maf.Vec2{
			0: {X: 1.5, Y: 0.5},
			1: {X: 1.5, Y: 1.5},
			2: {X: 1.7, Y: 0.5}, // No grid units.
			3: {X: 1.7, Y: 0.5}, // No grid units.
		},
		W: 2,
	})

	// Check the new grid maker accounts for seeds with no grid units.

	want, got := true, len(g.Cells) == 4 &&
		len(g.Cells[2].Units) == 0 && g.Cells[2].Units != nil &&
		len(g.Cells[3].Units) == 0 && g.Cells[3].Units != nil
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test multithreaded grid creation.

	g = boxyvor.NewGrid(boxyvor.GridSpec{
		NX: 100,
		NY: 100,
		Seeds: map[int]maf.Vec2{
			0: {X: 1.99, Y: 0.5},
			1: {X: 1.99, Y: 1.5},
			2: {X: 1.995, Y: 0.5}, // No grid units.
			3: {X: 1.995, Y: 0.5}, // No grid units.
		},
		W: 2,
	})

	want, got = true, len(g.Cells) == 4 &&
		len(g.Units) == 100*100 &&
		len(g.Cells[0].Units) == (100*100/2) &&
		len(g.Cells[1].Units) == (100*100/2) &&
		len(g.Cells[2].Units) == 0 && g.Cells[2].Units != nil &&
		len(g.Cells[3].Units) == 0 && g.Cells[3].Units != nil
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewRandomGrid(t *testing.T) {
	const (
		xUnits = 3
		yUnits = 6
		seeds  = 5
		physW  = 10
	)
	g := boxyvor.NewRandomGrid(boxyvor.RandomGridSpec{
		NX:    xUnits,
		NY:    yUnits,
		Seeds: seeds,
		W:     physW,
	})

	// Test the number of grid units is the product of the width and height in
	// grid units.

	want, got := xUnits*yUnits, len(g.Units)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test a grid has a number of cells equal to the number of seeds asked for.

	want, got = seeds, len(g.Cells)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test the seed positions are constrained to be within the physical grid
	// dimensions.

	wIn := interval.In{0, physW}
	hIn := interval.In{0, physW * float64(yUnits) / float64(xUnits)}
	for _, c := range g.Cells {
		p := c.P
		want, got := true,
			wIn.AsClosedContains(p.X) && hIn.AsClosedContains(p.Y)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test all grid units are accounted for in the Voronoi tessellation.

	{
		var n int
		for _, c := range g.Cells {
			vu := c.Units
			n += len(vu)
		}
		want, got := xUnits*yUnits, n
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestRepositionMany(t *testing.T) {
	g1 := boxyvor.NewRandomGrid(boxyvor.RandomGridSpec{
		NX:    20,
		NY:    20,
		Seeds: 10,
		W:     1,
	})
	midpoint := maf.Vec2{X: 0.5, Y: 0.5}

	s, _ := g1.AnySeed()

	g2 := boxyvor.RepositionMany(g1, map[int]maf.Vec2{
		s.ID: midpoint,
	})

	want, got := true, !g1.Equals(g2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	g1.Reposition(s.ID, midpoint)

	want, got = true, g1.Equals(g2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	s1, _ := g1.AnySeed()
	s2, _ := g1.AnySeed()

	g2 = boxyvor.RepositionMany(g1, map[int]maf.Vec2{
		s1.ID: {X: 0, Y: 0},
		s2.ID: {X: 1, Y: 1},
	})

	want, got = true, !g1.Equals(g2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	g1.Reposition(s1.ID, maf.Vec2{X: 0, Y: 0})
	g1.Reposition(s2.ID, maf.Vec2{X: 1, Y: 1})

	want, got = true, g1.Equals(g2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
