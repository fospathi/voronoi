module gitlab.com/fospathi/voronoi

go 1.21

toolchain go1.22.3

require (
	gitlab.com/fospathi/dryad v0.0.0-20230505044808-5afe126b1f0b
	gitlab.com/fospathi/maf v0.0.0-20230505232721-1284252f6d49
	gitlab.com/fospathi/universal v0.0.0-20230605055348-86065fb86769
)

require golang.org/x/exp v0.0.0-20231006140011-7918f672742d

replace gitlab.com/fospathi/dryad => ../dryad

replace gitlab.com/fospathi/maf => ../maf

replace gitlab.com/fospathi/universal => ../universal
